import subprocess
import argparse
import sys
from math import ceil

def get_audio_bitrate(file_path):
    """Extracts the bitrate of the audio file using ffmpeg."""
    command = ['ffmpeg', '-i', file_path, '-f', 'null', '-']
    result = subprocess.run(command, stderr=subprocess.PIPE, text=True)
    output = result.stderr
    lines = output.split('\n')
    bitrate_line = [x for x in lines if "bitrate:" in x]
    if not bitrate_line:
        raise ValueError("Bitrate information not found.")
    
    bitrate_info = bitrate_line[0].split()
    bitrate_index = bitrate_info.index("bitrate:") + 1
    bitrate_value = bitrate_info[bitrate_index]
    bitrate_unit = bitrate_value.split('k')[0]
    
    return float(bitrate_unit) * 1000  # Convert kbps to bps

def calculate_segment_length(bitrate, max_file_size):
    """Calculates maximum segment length in seconds."""
    return (max_file_size * 8) / bitrate  # max_file_size in bytes, bitrate in bps

def calculate_number_of_segments(file_duration, segment_length):
    """Calculates the number of segments needed for the audio file."""
    return int(ceil(file_duration / segment_length))

def get_audio_duration(file_path):
    """Gets the duration of the audio file using ffprobe."""
    command = [
        'ffprobe', '-v', 'error', '-show_entries',
        'format=duration', '-of', 'default=noprint_wrappers=1:nokey=1', file_path
    ]
    result = subprocess.run(command, stdout=subprocess.PIPE, text=True)
    return float(result.stdout.strip())

def main():
    parser = argparse.ArgumentParser(
        description="Estimate audio segments based on file size limits."
    )
    parser.add_argument("audio_file", help="Input audio file.")
    parser.add_argument(
        "--max_size", type=int, default=24999000,
        help="Max file size of each audio segment in bytes (default: 24,999,000)."
    )
    args = parser.parse_args()

    try:
        bitrate = get_audio_bitrate(args.audio_file)
        duration = get_audio_duration(args.audio_file)
        segment_length = calculate_segment_length(bitrate, args.max_size)
        num_segments = calculate_number_of_segments(duration, segment_length)

        print(f"Maximum segment length: {segment_length:.2f} seconds")
        print(f"Number of segments needed: {num_segments}")
    except Exception as e:
        print(f"Error processing the file: {e}", file=sys.stderr)
        sys.exit(1)

if __name__ == "__main__":
    if len(sys.argv) == 1:
        # Display help if no arguments provided
        subprocess.run(['python', __file__, '--help'])
    else:
        main()

