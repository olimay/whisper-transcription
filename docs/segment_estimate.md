# segment_estimate
This is the README for the `segment_estimate.py` script.

## Description

This Python script estimates the maximum length of audio segments such that
each segment remains under a specified file size. The default maximum file size
for each segment is 24,999,000 bytes (approximately 25 MB - 1 KB). The script
calculates the number of segments needed to split the entire audio file based
on this limit.

## Prerequisites

Before running the script, ensure you have `ffmpeg` and `ffprobe` installed on
your system. These tools are essential for analyzing the audio file's
properties such as bitrate and duration.

### Installation on Ubuntu
```bash
sudo apt install ffmpeg
```

### Installation on macOS
```bash
brew install ffmpeg
```

## Usage

To use the script, you must provide the path to an audio file. You can optionally specify the maximum file size for each segment.

### Basic Command
```bash
python segment_estimate.py <path_to_audio_file>
```

### Command with Optional Maximum File Size
```bash
python segment_estimate.py <path_to_audio_file> --max_size <max_size_in_bytes>
```

### Example
```bash
python segment_estimate.py example.mp3 --max_size 24999000
```

## Output

The script will output:
- The maximum segment length in seconds.
- The number of segments needed to split the audio file.

## Error Handling

If there is an error in processing the audio file, the script will print an
error message and exit. Possible errors include issues with reading the file or
retrieving necessary audio information.
