# split_audio
This is the README for the `split_audio.py` script.

## Description

`split_audio.py`, allows you to split audio files into fixed-length segments.
It's flexible enough to handle multiple audio formats and lets users specify
the segment length and units (minutes or seconds).

## Features

- **Support for Multiple Formats**: Handles popular audio formats like MP3,
  WAV, FLAC, AAC, OGG, and M4A.
- **Custom Segment Length**: Choose the length of each audio segment.
- **Unit Specification**: Specify the segment length units as either seconds or
  minutes.

## Prerequisites

Ensure you have Python installed on your system (Python 3.6+ recommended).
Additionally, you need the `pydub` module and `ffmpeg` installed to handle
various audio formats.

### Installation

Install the required Python module using pip:

```bash
pip install pydub
```

### ffmpeg Installation

`ffmpeg` is essential for processing most audio formats. Installation varies by
operating system:

- **Windows**: Download from [FFmpeg.org](https://ffmpeg.org/download.html) and
  add it to your system's PATH.
- **Mac**: Use Homebrew with the command `brew install ffmpeg`.
- **Linux**: Use your package manager, e.g., `sudo apt install ffmpeg` for
  Ubuntu.

## Usage

Run the script from the command line by specifying the audio file and optional
arguments for segment length and units.

### Basic Command Structure

```bash
python split_audio.py <filename> [--length <length>] [--units <units>]
```

### Examples

- **Split an audio file into 10-second segments** (default setting):

  ```bash
  python split_audio.py example.mp3
  ```

- **Split an audio file into 15-minute segments**:

  ```bash
  python split_audio.py example.flac --length 15 --units minutes
  ```

## Parameters

- `filename`: The path to the audio file you want to split.
- `--length`: The length of each segment. Default is 10 seconds.
- `--units`: The units of the segment length (`seconds` or `minutes`). Default
  is `seconds`.

## Output

The script outputs segmented audio files in the same directory as the original
file. Files are named sequentially (`SPLIT_001.mp3`, `SPLIT_002.mp3`, etc.)
according to their format.
