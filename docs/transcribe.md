# transcribe

This is the README for the `transcribe.py` script.

This Python script uses the OpenAI API to transcribe audio files from a specified
directory, matching the file name format "SPLIT_{###}.{ext}". The transcriptions
are saved into a subdirectory named "transcriptions" within the same directory.

## Features

- Transcribe audio files using the OpenAI Whisper model.
- Save transcriptions in text format with a filename corresponding to the audio
  file.
- Customizable directory input with a default option.
- API key is securely read from a specified file.

## Requirements

- Python 3.x
- openai Python library

You can install the required Python library using pip:

```bash
pip install openai
```

## Setup

1. Ensure you have Python installed on your system.
2. Install the `openai` library if not already installed.
3. Save the script to your desired directory.

## Usage

To run the script, you need to provide the path to your OpenAI API key file.
Optionally, you can specify the directory containing your audio files. If no
directory is specified, the script defaults to the current working directory.

Run the script with the following command:

```bash
python transcribe.py --api_key_file path/to/api_key.txt
```

Optionally, specify the audio file directory:

```bash
python transcribe.py --api_key_file path/to/api_key.txt --audio_dir path/to/audio/files
```

## Output

The script creates a "transcriptions" subdirectory within the specified audio
file directory and writes the transcriptions there. Each transcription file is
named to correspond to its audio file, following the format "SPLIT_{###}.txt".

## Troubleshooting

Ensure that:
- The API key file is correctly formatted and the key is valid.
- Audio files are accessible and in supported formats.
- The OpenAI library is up-to-date.

For more detailed errors, refer to the console outputs while running the script.

## License

This script is provided "as is", without warranty of any kind. Use it at your
own risk.
