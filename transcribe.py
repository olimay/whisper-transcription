import os
import argparse
import re
import openai


def load_api_key(filepath):
    with open(filepath, 'r') as file:
        return file.read().strip()


def transcribe_audio(file_path, prompt, client):
    with open(file_path, 'rb') as audio_file:
        transcription = client.audio.transcriptions.create(
            model="whisper-1",
            file=audio_file,
            language="en",
            response_format="text",
            prompt=prompt
        )
    return transcription


def main(directory, description_file, api_key_file):
    api_key = load_api_key(api_key_file)

    pattern = re.compile(r"SPLIT_(\d{3})\.\w+")
    transcription_dir = os.path.join(directory, 'transcriptions')
    os.makedirs(transcription_dir, exist_ok=True)

    with open(description_file, 'r') as prompt_file:
        prompt = prompt_file.read()

    client = openai.OpenAI(
        api_key=api_key
        )

    for filename in os.listdir(directory):
        match = pattern.match(filename)
        if match:
            number = match.group(1)
            file_path = os.path.join(directory, filename)
            print(f"Transcribing {filename}...")
            try:
                transcription = transcribe_audio(file_path, prompt, client)
                output_path = os.path.join(transcription_dir,
                                           f"SPLIT_{number}.txt")
                with open(output_path, 'w') as output_file:
                    output_file.write(transcription)
                print(f"Saved transcription to {output_path}")
            except Exception as e:
                print(f"Failed to transcribe {filename}: {e}")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='Transcribe audio files using OpenAI API.'
    )
    parser.add_argument('--api_key_file', required=True,
                        help='Path to the file containing the OpenAI API key.')
    parser.add_argument('--description_file', required=True,
                        help='Path to the file containing a description of the split audio.')
    parser.add_argument('--audio_dir', default=os.getcwd(),
                        help='Directory containing audio files. Defaults to the'
                             ' current directory if not specified.')
    args = parser.parse_args()

    main(args.audio_dir, args.description_file, args.api_key_file)

