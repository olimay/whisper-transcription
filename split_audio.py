import sys
import argparse
from pydub import AudioSegment

def split_audio(filename, segment_length, units):
    # Determine the file format based on the extension
    file_format = filename.split('.')[-1].lower()

    # List of acceptable formats
    supported_formats = ['mp3', 'wav', 'flac', 'aac', 'ogg', 'm4a']
    if file_format not in supported_formats:
        raise ValueError(f"Unsupported format: {file_format}. "
                         f"Supported: {', '.join(supported_formats)}.")

    # Convert segment length from units to milliseconds
    if units == 'minutes':
        segment_length_ms = segment_length * 60 * 1000
    elif units == 'seconds':
        segment_length_ms = segment_length * 1000
    else:
        raise ValueError("Invalid unit. Use 'seconds' or 'minutes'.")

    # Load and process the audio file
    audio = AudioSegment.from_file(filename, format=file_format)
    for i, chunk in enumerate(audio[::segment_length_ms]):
        # Format and export each segment
        segment_filename = f'SPLIT_{i+1:03d}.{file_format}'
        if file_format == 'm4a':
            chunk.export(segment_filename, format='ipod', codec='aac')
        else:
            chunk.export(segment_filename, format=file_format)
        print(f"Exported {segment_filename}")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='Split an audio file into segments.')
    parser.add_argument('filename', type=str, help='The audio file to split.')
    parser.add_argument('--length', type=int, default=10,
                        help='Segment length (default: 10).')
    parser.add_argument('--units', type=str, default='seconds',
                        choices=['seconds', 'minutes'],
                        help='Units for segment length (default: seconds).')
    args = parser.parse_args()

    try:
        split_audio(args.filename, args.length, args.units)
    except Exception as e:
        print(f"Error: {str(e)}")

