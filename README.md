# Whisper Transcription
Utilities for transcribing large audio files with the OpenAI Whisper API.


## Installation

```bash
git clone https://gitlab.com/olimay/whisper-transcription
cd whisper-transcription
```

## Usage

Each script can be used independently. See the `docs/` folder for detailed
information on usage and dependencies.

## License

This project is licensed under the Mozilla Public License 2.0 (MPL-2.0) - see
the [LICENSE](LICENSE) file for details.
