#!/bin/bash

# Set default directory to the current working directory if not provided
directory="${1:-$(pwd)}"

# Set default output file to 'combined.txt' in the given directory if not provided
output_file="${2:-$(pwd)/combined.txt}"

# Go to the directory
cd "$directory"

# Ensure the output file is empty
> "$output_file"

# Sort files and process each one
for file in $(ls SPLIT_*.txt | sort -t '_' -k 2 -n); do
    # Write the filename to the output file
    echo "File: $file" >> "$output_file"
    # Append the file's content to the output file
    cat "$file" >> "$output_file"
done

echo "Files have been combined successfully. Output at: $output_file"

